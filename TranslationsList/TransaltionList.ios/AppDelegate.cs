﻿using Foundation;
using MvvmCross.Platforms.Ios.Core;
using UIKit;

namespace TransaltionList.ios
{
    [Register("AppDelegate")]
    public partial class AppDelegate : MvxApplicationDelegate<Setup, TranslationsList.Core.App>
    {
        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            var result = base.FinishedLaunching(application, launchOptions);

            return result;
        }
    }
}

