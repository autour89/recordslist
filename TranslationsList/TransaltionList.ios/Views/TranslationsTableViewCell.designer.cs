﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TransaltionList.ios.Views
{
    [Register ("TranslationsTableViewCell")]
    partial class TranslationsTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Translation1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Tsanslation2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Word { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Translation1 != null) {
                Translation1.Dispose ();
                Translation1 = null;
            }

            if (Tsanslation2 != null) {
                Tsanslation2.Dispose ();
                Tsanslation2 = null;
            }

            if (Word != null) {
                Word.Dispose ();
                Word = null;
            }
        }
    }
}