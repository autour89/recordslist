﻿using System;
using Foundation;
using MvvmCross.Platforms.Ios.Views;
using MvvmCross.ViewModels;

namespace TransaltionList.ios.Views
{
    public class BaseView<TViewModel> : MvxBaseViewController<TViewModel> where TViewModel : class, IMvxViewModel
    {
        public BaseView()
        {
        }

        protected BaseView(string nibName, NSBundle bundle) : base(nibName, bundle)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if(NavigationController != null)
            {
                NavigationController.NavigationBar.Translucent = false;
            }
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

    }
}
