﻿using System;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using TranslationsList.Core.Services.DataService.DataModel;
using UIKit;

namespace TransaltionList.ios.Views
{
    public partial class TranslationsTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString(nameof(TranslationsTableViewCell));
        public static readonly UINib Nib;

        static TranslationsTableViewCell()
        {
            Nib = UINib.FromName(nameof(TranslationsTableViewCell), NSBundle.MainBundle);
        }

        protected TranslationsTableViewCell(IntPtr handle) : base(handle)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            SelectionStyle = UITableViewCellSelectionStyle.None;

            this.DelayBind(InitBindings);
        }

        private void InitBindings()
        {
            var set = this.CreateBindingSet<TranslationsTableViewCell, Translation> ();
            set.Bind(Word).For(v => v.Text).To(vm => vm.Record);
            set.Bind(Translation1).For(v => v.Text).To(vm => vm.Translation1);
            set.Bind(Tsanslation2).For(v => v.Text).To(vm => vm.Translation2);
            set.Apply();
        }
    }
}
