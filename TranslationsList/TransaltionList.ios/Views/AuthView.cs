﻿using System;
using UIKit;
using TranslationsList.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;

namespace TransaltionList.ios.Views
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public partial class AuthView : BaseView<AuthViewModel>
    {
        public AuthView() : base(nameof(AuthView), null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            InitBindings();
        }

        private void InitView()
        {

        }

        private void InitBindings()
        {
            var set = this.CreateBindingSet<AuthView, AuthViewModel>();
            set.Bind(_loginField).To(vm => vm.Login);
            set.Bind(_passwordField).To(vm => vm.Password);
            set.Bind(_authButton.Tap()).For(v => v.Command).To(vm => vm.AuthenticateCommand);
            set.Apply();
        }
    }
}

