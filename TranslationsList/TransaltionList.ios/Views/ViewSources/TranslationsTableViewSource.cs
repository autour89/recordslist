﻿using System;
using Foundation;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;

namespace TransaltionList.ios.Views.ViewSources
{
    public class TranslationsTableViewSource : MvxTableViewSource
    {
        protected string CellIdentifier => TranslationsTableViewCell.Key;

        public TranslationsTableViewSource(UITableView tableView) : base(tableView)
        {
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.RegisterNibForCellReuse(TranslationsTableViewCell.Nib, CellIdentifier);
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return tableView.DequeueReusableCell(CellIdentifier);
        }
    }
}
