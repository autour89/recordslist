﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TransaltionList.ios.Views
{
    [Register ("HomeView")]
    partial class HomeView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AddButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView AddRemoveTabView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UINavigationItem CustomNavigationItem { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton LogoutButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView LogoutTabView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem MenuButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PasswordTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton RemoveButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISegmentedControl SegmentedControl { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel UsernameTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView UserTabView { get; set; }

        [Action ("SegmenteContol_ValueChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SegmenteContol_ValueChanged (UIKit.UISegmentedControl sender);

        void ReleaseDesignerOutlets ()
        {
            if (AddButton != null) {
                AddButton.Dispose ();
                AddButton = null;
            }

            if (AddRemoveTabView != null) {
                AddRemoveTabView.Dispose ();
                AddRemoveTabView = null;
            }

            if (CustomNavigationItem != null) {
                CustomNavigationItem.Dispose ();
                CustomNavigationItem = null;
            }

            if (LogoutButton != null) {
                LogoutButton.Dispose ();
                LogoutButton = null;
            }

            if (LogoutTabView != null) {
                LogoutTabView.Dispose ();
                LogoutTabView = null;
            }

            if (MenuButton != null) {
                MenuButton.Dispose ();
                MenuButton = null;
            }

            if (PasswordTitle != null) {
                PasswordTitle.Dispose ();
                PasswordTitle = null;
            }

            if (RemoveButton != null) {
                RemoveButton.Dispose ();
                RemoveButton = null;
            }

            if (SegmentedControl != null) {
                SegmentedControl.Dispose ();
                SegmentedControl = null;
            }

            if (TableView != null) {
                TableView.Dispose ();
                TableView = null;
            }

            if (UsernameTitle != null) {
                UsernameTitle.Dispose ();
                UsernameTitle = null;
            }

            if (UserTabView != null) {
                UserTabView.Dispose ();
                UserTabView = null;
            }
        }
    }
}