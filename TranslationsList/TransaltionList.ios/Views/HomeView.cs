﻿using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using TransaltionList.ios.Views.ViewSources;
using TranslationsList.Core.ViewModels;
using UIKit;

namespace TransaltionList.ios.Views
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public partial class HomeView : BaseView<HomeViewModel>
    {
        private bool _hamburgerMenuIsVisible = false;
        private TranslationsTableViewSource _source;

        public override UINavigationItem NavigationItem => CustomNavigationItem;

        public HomeView() : base(nameof(HomeView), null)
        {

        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            InitView();
            InitBindings();
        }

        partial void SegmenteContol_ValueChanged(UISegmentedControl sender)
        {
            switch (SegmentedControl.SelectedSegment)
            {
                case 0:
                    UserTabView.Hidden = false;
                    AddRemoveTabView.Hidden = true;
                    LogoutTabView.Hidden = true;
                    break;
                case 1:
                    UserTabView.Hidden = true;
                    AddRemoveTabView.Hidden = false;
                    LogoutTabView.Hidden = true;
                    break;
                case 2:
                    UserTabView.Hidden = true;
                    AddRemoveTabView.Hidden = true;
                    LogoutTabView.Hidden = false;
                    break;
            }
        }

        private void InitView()
        {
            EdgesForExtendedLayout = UIRectEdge.None;
            TableView.TableFooterView = new UIView();
            _source = new TranslationsTableViewSource(TableView);
            TableView.Source = _source;
            MenuButton.Clicked += MenuClickedAction;
        }

        private void MenuClickedAction(object sender, EventArgs e)
        {
            if (!_hamburgerMenuIsVisible)
            {
                //Unhide the menu                                           
                //leadingConstraint.Constant = 150;
                //trailingConstraint.Constant = -(150);
                _hamburgerMenuIsVisible = true;
            }
            else
            {
                //Hide the menu                                        
                //leadingConstraint.Constant = 0;
                //trailingConstraint.Constant = 0;
                _hamburgerMenuIsVisible = false;
            }
            //Animate the hiding and unhiding                       
            UIView.Animate(0.3, 0, UIViewAnimationOptions.CurveEaseIn, AnimationAction, AnimationCompletionHandler);
        }

        private void AnimationAction()
        {
            this.View.LayoutIfNeeded();
        }


        private void AnimationCompletionHandler()
        {
            //Completion of Animation                   
        }

        private void InitBindings()
        {
            var set = this.CreateBindingSet<HomeView, HomeViewModel>();
            set.Bind(_source).To(vm => vm.Translations);
            set.Bind(AddButton.Tap()).For(v => v.Command).To(vm => vm.AddCommand);
            set.Bind(RemoveButton.Tap()).For(v => v.Command).To(vm => vm.RemoveCommand);
            set.Bind(LogoutButton.Tap()).For(v => v.Command).To(vm => vm.LogoutCommand);
            set.Bind(UsernameTitle).For(v => v.Text).To(vm => vm.Login);
            set.Bind(PasswordTitle).For(v => v.Text).To(vm => vm.Password);
            set.Apply();
        }
    }
}

