// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TransaltionList.ios.Views
{
    [Register ("AuthView")]
    partial class AuthView
    {
        [Outlet]
        UITextField _loginField { get; set; }

        [Outlet]
        UITextField _passwordField { get; set; }

        [Outlet]
        UIButton _authButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_authButton != null) {
                _authButton.Dispose ();
                _authButton = null;
            }

            if (_loginField != null) {
                _loginField.Dispose ();
                _loginField = null;
            }

            if (_passwordField != null) {
                _passwordField.Dispose ();
                _passwordField = null;
            }
        }
    }
}