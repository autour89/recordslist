﻿using System;
using System.Threading.Tasks;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using TranslationsList.Core.Services;
using TranslationsList.Core.ViewModels;

namespace TranslationsList.Core
{
    public class AppStart : MvxAppStart
    {
        private readonly IAuthService _authService;
        

        public AppStart(
                IMvxApplication application,
                IMvxNavigationService navigationService,
                IAuthService authService) : base(application, navigationService)
                {
                    _authService = authService;
                }

        protected override async Task NavigateToFirstViewModel(object hint = null)
        {
            if (_authService.CurrentUser != null )
            {
                await NavigationService.Navigate<HomeViewModel>();
            }
            else
            {
                await NavigationService.Navigate<AuthViewModel>();
            }
        }
    }
}
