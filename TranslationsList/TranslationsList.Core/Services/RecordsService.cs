﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TranslationsList.Core.Services.DataService.DataContext;
using TranslationsList.Core.Services.DataService.DataModel;

namespace TranslationsList.Core.Services
{
    public interface IRecordsService
    {
        /// <summary>
        /// Gets the records.
        /// </summary>
        List<Translation> Records { get; }

        /// <summary>
        /// Adds the record.
        /// </summary>
        Task AddRecord(Translation translationModel);

        /// <summary>
        /// Removes the record.
        /// </summary>
        Task RemoveRecord(Translation translationModel);
    }

    public class RecordsService : IRecordsService
    {
        private readonly IApplicationContextService _applicationContextService;
        private readonly IAuthService _authService;

        public RecordsService(IApplicationContextService applicationContextService, IAuthService authService)
        {
            _applicationContextService = applicationContextService;
            _authService = authService;
        }

        public List<Translation> Records
        {
            get
            {
                var user = _authService.CurrentUser;

                var records = _applicationContextService.Translations.Where(f => f.OwnerId == user.Id).ToList();

                return records;
            }
        }

        public Task AddRecord(Translation record)
        {
            record.OwnerId = _authService.CurrentUser.Id;
            _applicationContextService.Translations.Add(record);
            _applicationContextService.Context.SaveChanges();

            return Task.CompletedTask;
        }

        public Task RemoveRecord(Translation record)
        {
            _applicationContextService.Translations.Remove(record);
            _applicationContextService.Context.SaveChanges();

            return Task.CompletedTask;
        }
    }
}
