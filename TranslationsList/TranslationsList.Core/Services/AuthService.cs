﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TranslationsList.Core.Services.DataService.DataContext;
using TranslationsList.Core.Services.DataService.DataModel;

namespace TranslationsList.Core.Services
{
    public interface IAuthService
    {
        /// <summary>
        /// Authenticates user.
        /// </summary>
        Task<bool> AuthenticateAsync(string login, string password);

        /// <summary>
        /// Logout from the session.
        /// </summary>
        Task<bool> LogOutAsync();

        /// <summary>
        /// Gets the current user.
        /// </summary>
        User CurrentUser { get; }

        /// <summary>
        /// Gets the last loged in user.
        /// </summary>
        User LastLogedInUser { get; }
    }

    public class AuthService : IAuthService
    {
        private readonly IApplicationContextService _applicationContextService;

        public AuthService(IApplicationContextService applicationContextService)
        {
            _applicationContextService = applicationContextService;
        }

        public User CurrentUser
        {
            get
            {
                return _applicationContextService.Users.FirstOrDefault(u => u.LoggedIn);
            }
        }

        public User LastLogedInUser => _applicationContextService.Users.OrderByDescending(date => date.LastLogedIn).FirstOrDefault();

        public Task<bool> AuthenticateAsync(string login, string password)
        {
            if (string.IsNullOrEmpty(login) || string.IsNullOrWhiteSpace(login) || string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password))
            {
                return Task.FromResult(false);
            }

            var existingUser = _applicationContextService.Users.FirstOrDefault(x=>x.Login == login && x.Password == password);

            if (existingUser != null)
            {
                existingUser.LoggedIn = true;
                existingUser.LastLogedIn = DateTime.UtcNow;
                _applicationContextService.Users.Update(existingUser);
            }
            else
            {
                var newUser = new User
                {
                    Login = login,
                    Password = password,
                    LoggedIn = true,
                    CreatedAt = DateTime.UtcNow,
                    LastLogedIn = DateTime.UtcNow
                };
                _applicationContextService.Users.Add(newUser);
            }
            _applicationContextService.Context.SaveChanges();

            return Task.FromResult(true);
        }

        public Task<bool> LogOutAsync()
        {
            var user = CurrentUser;

            if (user == null)
            {
                return Task.FromResult(false);
            }

            user.LoggedIn = false;
            _applicationContextService.Users.Update(user);
            _applicationContextService.Context.SaveChanges();

            return Task.FromResult(true);
        }
    }
}
