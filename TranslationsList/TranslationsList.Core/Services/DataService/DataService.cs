﻿//using System.Collections.Generic;
//using System.Threading.Tasks;
//using SQLite;
//using TranslationsList.Core.Services.DataService.DataContext;
//using TranslationsList.Core.Services.DataService.DataModel;

//namespace TranslationsList.Core.Services.DataService
//{
//    public interface IDataService
//    {
//        Task InsertAsync<T>(T data) where T : BaseEntity;
//        Task InsertAsync<T>(List<T> datatList) where T : BaseEntity;
//        Task InsertOrUpdateAsync<T>(T data) where T : BaseEntity;
//        Task UpdateAsync<T>(T data) where T : BaseEntity;
//        Task DeleteAsync<T>(T data) where T : BaseEntity;
//        Task<List<T>> SelectAsync<T>(string query) where T : new();
//        Task<List<T>> GetAllAsync<T>() where T : BaseEntity, new();

//        IList<T> SelectAll<T>() where T : BaseEntity, new();
//        void Insert<T>(T data) where T : BaseEntity;
//        void Insert<T>(List<T> data) where T : BaseEntity;
//        void InsertOrUpdate<T>(T data) where T : BaseEntity;
//        void Update<T>(T data) where T : BaseEntity;
//        void Delete<T>(T daata) where T : BaseEntity;
//        void DeleteAll<T>() where T : BaseEntity;
//    }

//    public class DataService : IDataService
//    {
//        private readonly IPath _path;

//        public SQLiteAsyncConnection ConnectionAsync { get; }
//        public SQLiteConnection Connection { get; }

//        public DataService(IPath path)
//        {
//            _path = path;
//            ConnectionAsync = new SQLiteAsyncConnection(path.GetDatabasePath(Configuration.DBFilename));
//            Connection = new SQLiteConnection(path.GetDatabasePath(Configuration.DBFilename));
//            PrepareTables();
//        }

//        public async Task InsertAsync<T>(T data) where T : BaseEntity
//        {
//            await ConnectionAsync.InsertAsync(data);
//        }

//        public async Task InsertAsync<T>(List<T> data) where T : BaseEntity
//        {
//            await ConnectionAsync.InsertAllAsync(data);
//        }

//        public async Task InsertOrUpdateAsync<T>(T data) where T : BaseEntity
//        {
//            await ConnectionAsync.InsertOrReplaceAsync(data);
//        }

//        public async Task UpdateAsync<T>(T data) where T : BaseEntity
//        {
//            await ConnectionAsync.UpdateAsync(data);
//        }

//        public async Task DeleteAsync<T>(T data) where T : BaseEntity
//        {
//            await ConnectionAsync.DeleteAsync(data);
//        }

//        public async Task<List<T>> SelectAsync<T>(string query) where T : new()
//        {
//            return await ConnectionAsync.QueryAsync<T>(query);
//        }

//        public async Task<List<T>> GetAllAsync<T>() where T : BaseEntity, new()
//        {
//            return await ConnectionAsync.Table<T>().ToListAsync();
//        }

//        public IList<T> SelectAll<T>() where T : BaseEntity, new()
//        {
//            return Connection.Table<T>().ToList();
//        }

//        public void Insert<T>(T data) where T : BaseEntity
//        {
//            Connection.Insert(data);
//        }

//        public void Insert<T>(List<T> data) where T : BaseEntity
//        {
//            Connection.InsertAll(data);
//        }

//        public void InsertOrUpdate<T>(T data) where T : BaseEntity
//        {
//            Connection.InsertOrReplace(data);
//        }

//        public void Update<T>(T data) where T : BaseEntity
//        {
//            Connection.Update(data);
//        }

//        public void Delete<T>(T data) where T : BaseEntity
//        {
//            Connection.Delete(data);
//        }

//        public void DeleteAll<T>() where T : BaseEntity
//        {
//            Connection.DeleteAll<T>();
//        }

//        private void PrepareTables()
//        {
//            Connection.CreateTable<User>();
//            Connection.CreateTable<Translation>();
//        }

//    }
//}
