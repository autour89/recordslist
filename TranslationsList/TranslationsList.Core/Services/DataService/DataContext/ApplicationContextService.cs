﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using TranslationsList.Core.Services.DataService.DataModel;
using Xamarin.Essentials;

namespace TranslationsList.Core.Services.DataService.DataContext
{
    public interface IApplicationContextService : ICurrentDbContext
    {
        DbSet<User> Users { get; set; }

        DbSet<Translation> Translations { get; set; }
    }

    public class ApplicationContextService : DbContext, IApplicationContextService
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Translation> Translations { get; set; }

        public DbContext Context => this;

        public ApplicationContextService()
        {
            Task.Run(Init);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = new SqliteConnectionStringBuilder()
            {
                DataSource = Path.Combine(FileSystem.AppDataDirectory, Configuration.DBFilename),
                Mode = SqliteOpenMode.ReadWriteCreate,
            }.ToString();

            optionsBuilder.UseSqlite(connectionString);
        }

        private async Task Init()
        {
            SQLitePCL.Batteries.Init();
            Database.OpenConnection();
            Database.EnsureCreated();
            await Database.MigrateAsync(); //We need to ensure the latest Migration was added. This is different than EnsureDatabaseCreated.
        }
    }
}
