﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TranslationsList.Core.Services.DataService.DataModel
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
