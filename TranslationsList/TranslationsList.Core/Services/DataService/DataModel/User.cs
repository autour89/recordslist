﻿using System;

namespace TranslationsList.Core.Services.DataService.DataModel
{
    public class User : BaseEntity
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public bool LoggedIn { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime LastLogedIn { get; set; }
    }


}
