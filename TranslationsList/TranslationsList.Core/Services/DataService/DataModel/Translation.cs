﻿
namespace TranslationsList.Core.Services.DataService.DataModel
{
    public class Translation : BaseEntity
    {
        public int OwnerId { get; set; }

        public string Record { get; set; }

        public string Translation1 { get; set; }

        public string Translation2 { get; set; }

    }
}
