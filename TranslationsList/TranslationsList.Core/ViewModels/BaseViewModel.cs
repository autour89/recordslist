﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace TranslationsList.Core.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
        protected abstract Task LoadAsync();

        private bool _loading;
        public bool Loading
        {
            get => _loading;
            set => SetProperty(ref _loading, value);
        }

        public MvxAsyncCommand ReloadCommand { get; private set; }

        protected BaseViewModel()
        {
            ReloadCommand = new MvxAsyncCommand(LoadAsync);
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            ReloadCommand.Execute(null);
        }
    }

    public abstract class BaseViewModel<TParam> : BaseViewModel, IMvxViewModel<TParam>
    {
        private TParam _parameter;
        public TParam Parameter
        {
            get => _parameter;
            set => SetProperty(ref _parameter, value);
        }

        public void Prepare(TParam parameter)
        {
            Parameter = parameter;
        }
    }
}
