﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using TranslationsList.Core.Services;

namespace TranslationsList.Core.ViewModels
{
    public class AuthViewModel : BaseViewModel
    {
        private readonly IMvxNavigationService _mvxNavigationService;
        private readonly IAuthService _authService;

        public AuthViewModel(IMvxNavigationService mvxNavigationService, IAuthService authService)
        {
            _mvxNavigationService = mvxNavigationService;
            _authService = authService;

            AuthenticateCommand = new MvxAsyncCommand(Authenticate);
        }

        private string _login;
        public string Login
        {
            get => _login;
            set => SetProperty(ref _login, value);
        }

        private string _password;
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public MvxAsyncCommand AuthenticateCommand { get; private set; }

        protected override Task LoadAsync()
        {
            return Task.CompletedTask;
        }

        private async Task Authenticate()
        {
            Loading = true;
            if(await _authService.AuthenticateAsync(Login, Password))
            {
                await _mvxNavigationService.Navigate<HomeViewModel>();
            }
            Loading = false;
        }

    }
}
