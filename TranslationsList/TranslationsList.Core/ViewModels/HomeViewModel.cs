﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using TranslationsList.Core.Services;
using TranslationsList.Core.Services.DataService.DataModel;

namespace TranslationsList.Core.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private readonly IMvxNavigationService _mvxNavigationService;
        private readonly IAuthService _authService;
        private readonly IRecordsService _recordsService;

        private MvxObservableCollection<Translation> _translations;
        public MvxObservableCollection<Translation> Translations
        {
            get => _translations;
            set => SetProperty(ref _translations, value);
        }

    #region user info

        public string Login { get; private set; }

        public string Password { get; private set; }

    #endregion user info

        public MvxAsyncCommand AddCommand { get; private set; }

        public MvxCommand RemoveCommand { get; private set; }

        public MvxAsyncCommand LogoutCommand { get; private set; }

        public HomeViewModel(IMvxNavigationService mvxNavigationService, IAuthService authService, IRecordsService recordsService)
        {
            _mvxNavigationService = mvxNavigationService;
            _authService = authService;
            _recordsService = recordsService;
            AddCommand = new MvxAsyncCommand(AddTranslation);
            RemoveCommand = new MvxCommand(RemoveTranslation);
            LogoutCommand = new MvxAsyncCommand(DoLogout);
        }

        private async Task AddTranslation()
        {
            var random = new Random();
            var newRecord = new Translation
            {
                Record = random.Next(1, 1000).ToString(),
                Translation1 = random.Next(1, 1000).ToString(),
                Translation2 = random.Next(1, 1000).ToString()
            };

            Translations.Add(newRecord);
            await _recordsService.AddRecord(newRecord);
        }

        private void RemoveTranslation()
        {
            var toRemove = Translations.FirstOrDefault();

            if( toRemove != null)
            {
                Translations.Remove(toRemove);
                _recordsService.RemoveRecord(toRemove);
            }
        }

        private async Task DoLogout()
        {
            await _authService.LogOutAsync();
            await _mvxNavigationService.Navigate<AuthViewModel>();
        }

        protected override Task LoadAsync()
        {
            var user = _authService.CurrentUser;
            Login = user?.Login;
            Password = user?.Password;

            Translations = new MvxObservableCollection<Translation>(_recordsService.Records);

            return Task.CompletedTask;
        }

    }
}
